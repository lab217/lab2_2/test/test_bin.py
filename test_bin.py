from src.app import util

solution = [0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ,12 ,13]
elem = 11


def test_correctness_search():
    result = util.binary_search(sorted(solution), elem)
    assert result == solution.index(11)


def test_correctness_input_data():
    result = util.binary_search(sorted(solution), elem)
    assert result != str(result)
